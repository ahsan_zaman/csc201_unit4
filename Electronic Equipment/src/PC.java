
public class PC extends Computer{
	private String motherboard;
	
	public PC( String processor, String motherboard ){
		this.processor = processor;
		this.motherboard = motherboard;
	}
	
	public String toString(){
		return "Processor: "+processor+"\nMotherboard: "+motherboard;
	}
	
	public String getMotherboard(){
		return motherboard;
	}
}

