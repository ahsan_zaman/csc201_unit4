
public class Smartphones extends Computer{
	private float screenSize;
	private float weight;
	
	public Smartphones( String processor, float screenSize, float weight ){
		this.processor = processor;
		this.screenSize = screenSize;
		this.weight = weight;
	}
	
	public String toString(){
		return "Processor: "+processor+"\nScreen Size: "+screenSize+"\nWeight: "+weight;
	}
	
	public float getScreenSize(){
		return screenSize;
	}
	
	public float getWeight(){
		return weight;
	}
}
