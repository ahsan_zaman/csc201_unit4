
public class DigitalCamera extends Cameras{
	private float weight;
	
	public DigitalCamera( float megaPixels, float weight ){
		this.megaPixels = megaPixels;
		this.weight = weight;
	}
	
	public String toString(){
		return "MegaPixels: "+megaPixels+"\nWeight: "+weight;
	}
	
	public float getWeight(){
		return weight;
	}
}
