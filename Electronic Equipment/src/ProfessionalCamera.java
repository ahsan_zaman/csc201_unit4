
public class ProfessionalCamera extends Cameras{
	private float focalLength;
	
	public ProfessionalCamera( float megaPixels, float focalLength ){
		this.megaPixels = megaPixels;
		this.focalLength = focalLength;
	}
	
	public String toString(){
		return "MegaPixels: "+megaPixels+"\nFocal Length: "+focalLength;
	}
	
	public float getFocal(){
		return focalLength;
	}
}
