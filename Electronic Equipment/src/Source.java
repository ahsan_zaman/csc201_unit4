/**
 * 
 * @author Ahsan Zaman
 * CSC 201 - Unit 4
 * 
 */
import java.util.Scanner;
public class Source {
	public enum Menu{
		EXIT, PC, SMARTPHONE, DIGITAL_CAMERA, PRO_CAMERA;
	}
	public static void main( String[] args ){
		Scanner input = new Scanner(System.in);
		int choice=-1;									// For user menu
		float firstValue, secondValue;					// General floating point variables for multipurpose use in this program
		String processor, motherboard;
		
		System.out.println( "What do you have:\n1. PC\n2. Smartphone\n3. Digital Camera\n4. Professional Camera\nPress 0 to exit." );
		choice = input.nextInt();
		input.nextLine();
		switch( Menu.values()[choice] ){
		case EXIT:
			System.out.println( "Exit sequence." );
			break;
		case PC:
			System.out.println( "Enter the processor for your PC: " );
			processor = input.nextLine();
			System.out.println( "Enter the motherboard: " );
			motherboard = input.nextLine();
			PC myHP = new PC( processor, motherboard );
			System.out.println( myHP.toString() );		// Displaying user input
			break;
		case SMARTPHONE:
			System.out.println( "Enter the processor in your smartphone: " );
			processor = input.nextLine();
			System.out.println( "Enter the diagonal Screen size of your phone: " );
			firstValue = input.nextFloat();				// Storing screen size here
			System.out.println( "Enter the weight of your smartphone: " );
			secondValue = input.nextFloat();			// storing weight of phone here
			Smartphones S5 = new Smartphones( processor, firstValue, secondValue );
			System.out.println( S5.toString() );
			break;
		case DIGITAL_CAMERA:
			System.out.println( "Enter the megapixels of your camera: " );
			firstValue = input.nextFloat();				// storing megapixels
			System.out.println( "Enter weight of the camera: " );
			secondValue = input.nextFloat();			// storing weight
			DigitalCamera Sony = new DigitalCamera( firstValue, secondValue );
			System.out.println( Sony.toString() );
			break;
		case PRO_CAMERA:
			System.out.println( "Enter the megapixels of your camera: " );
			firstValue = input.nextFloat();				// storing megapixels of the camera
			System.out.println( "Enter the focal length your camera (in mm): " );
			secondValue = input.nextFloat();			// focal length of the camera on which depends range and quality
			ProfessionalCamera Canon = new ProfessionalCamera( firstValue, secondValue );
			System.out.println( Canon.toString() );
			break;
		}
		
		input.close();
	}
}
