/**
 * @author Ahsan Zaman
 * CSC 201 - Unit 4
 * Sports
 * 
 * Algorithm:
 * 1. Declare and Initialize integer variables choice, players, number.
 * 2. Give user the menu and ask for input
 * 3. Store input in choice and use it in the switch case
 * 4. If the user chose football then, 
 * 		5. Prompt user to enter the number of players and points scored.
 * 		6. Display what the user entered.
 * 7. If the user chose Soccer then,
 * 		8. Prompt user to enter the number of players and goals scored in the game.
 * 		9. Display what the user entered.
 * 10. If the user chose Cricket then,
 * 		11. Prompt user to enter the number of players and runs scored in the game.
 * 		12. Display what the user entered.
 * 11. If the user chose to exit the program, then choice=0
 * END
 */
import java.util.Scanner;
public class Test {
	
	public enum Menu{
		EXIT, AMERICAN, SOCCER, CRICKET;
	}
	
	public static void main( String[] args ){
		Scanner input = new Scanner(System.in);
		int choice=-1, players=0, number=0;							/**Choice is for menu, players and number are going to be inputs by the user*/
	
		while( choice!=0 ){
			System.out.println( "\t\t**Sports**\nWhich sport do wish to play:\n"
					+ "1. Football\n2. Soccer\n3. Cricket\nPress 0 to exit." );
			choice = input.nextInt();
			
			switch( Menu.values()[choice] ){						// Using enum for menu choice
			
			case AMERICAN:
				System.out.println( "Enter the number of players: " );
				players = input.nextInt();
				System.out.println( "Enter the points earned: " );
				number = input.nextInt();
				American object1 = new American( players, number );
				object1.display();
				break;
			
			case SOCCER:
				System.out.println( "Enter the number of players: " );
				players = input.nextInt();
				System.out.println( "Enter the goals scored: " );
				number = input.nextInt();
				Soccer object2 = new Soccer( players, number );
				object2.display();
				break;
			
			case CRICKET:
				System.out.println( "Enter the number of players: " );
				players = input.nextInt();
				System.out.println( "Enter the runs scored: " );
				number = input.nextInt();
				Cricket object3 = new Cricket( players, number );
				object3.display();
				break;
			
			case EXIT:
				System.out.println( "Exit sequence initiated. " );
				break;
				default: System.out.println( "Incorrect Entry. Please try again." );
			}
			
		}
		input.close();
	}
}
