public abstract class Football extends Sports{
	protected int points;
	
	public Football(){
		players = 0;
		points = 0;
	}
	
	public void display(){
		System.out.println( "\nNumber of players: "+players+"\nPoints: "+points );
	}
}