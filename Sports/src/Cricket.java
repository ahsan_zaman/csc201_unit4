public class Cricket extends Sports {
	private int score;
	
	public Cricket(){
		score = 0;
	}
	/** @param Initializing number of players in Cricket*/
	/** @param Initializing the runs score by the players*/
	public Cricket( int players, int score ){		
		this.players = players;						
		this.score = score;
	}
	
	public void display(){
		System.out.println( "Players: "+players+"\nScore: "+score );
	}
}
