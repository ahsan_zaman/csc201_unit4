public class Soccer extends Football{
	private int goals;
	/** @param Initializing number of players in Soccer*/
	/** @param Initializing goals scored by the players*/
	public Soccer( int players, int goals ){		
		this.players = players;						
		this.goals = goals;
	}
		
	public void display(){
		System.out.println( "Players: "+players+"\nGoals: "+goals );
	}
}
	