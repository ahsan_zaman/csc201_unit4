	public class Book extends Literature{
		protected String authorName;
	
		public Book(){
			authorName = null;
		}
		
		/**@param Initializing authorName with user input
		 * @param Initializing numberOfPages with user input
		 * @param Initializing title with user input */
		public Book( String authorName, int numberOfPages, String title ){
			this.authorName = authorName;
			this.numberOfPages = numberOfPages;
			this.title = title;
		}
	
		public void displayBook(){
			System.out.println( "Author Name: "+authorName+"\nNumber of Pages: "+numberOfPages+"\nTitle: "+title );
		}
	}
