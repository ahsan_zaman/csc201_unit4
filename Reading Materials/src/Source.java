/**
 * @author Ahsan Zaman
 * CSC 201 - Unit 4
 * Reading Materials
 * 
 * Algorithm:
 * 
 */
import java.util.Scanner;
public class Source {
	public enum Menu{
		EXIT, MAGAZINE, TEXTBOOK, NOVEL;
	}

	public static void main( String[] args ){
		Scanner input = new Scanner(System.in);
		int choice =-1, pages=0;
		double price=0.0;
		String author, title;
		
		/**choice is for user menu input
		 * pages, price, author and title will be the values the user will enter to be passed into the respective objects*/
		while( choice!=0 ){
			System.out.println( "\t\tReading Material\nWhat do you wish to buy: "
					+ "\n1. Magazine\n2. Textbook\n3. Novel\nPress 0 to exit. " );
			choice = input.nextInt();
			input.nextLine();
			switch( Menu.values()[choice] ){
			case EXIT:
				System.out.println( "Exit sequence initiated. " );
				break;
			
			case MAGAZINE:
				System.out.println( "Enter Name of magazine: " );
				title = input.nextLine();
				System.out.println( "Enter Price of the magazine: " );
				price = input.nextDouble();
				System.out.println( "Enter number of pages in this magazine: " );
				pages = input.nextInt();
				input.nextLine();
				Magazine zine = new Magazine( pages, title, price);				// Passing values into Magazine object through constructor
				zine.display();
				break;
					
			case TEXTBOOK:
				System.out.println( "Enter the number of pages in the textbook: " );
				pages = input.nextInt();
				input.nextLine();
				System.out.println( "Enter the price of the textbook: " );
				price = input.nextDouble();
				Textbook tbk = new Textbook( pages, price );					// Passing values into Textbook object through constructor
				tbk.display();
				break;
					
			case NOVEL:
				System.out.println( "Enter the number of pages in the Novel: " );
				pages = input.nextInt();
				input.nextLine();
				System.out.println( "Enter the Author's name: " );
				author = input.nextLine();
				System.out.println( "Enter the Price of novel: " );
				price = input.nextDouble();
				System.out.println( "Enter the Title of the novel: " );
				title = input.nextLine();
				Novel bk = new Novel( author, price , title, pages );			// Passing values into Novel object through constructor
				bk.display();
				break;
					
				default: System.out.println( "Incorrect choice. Please try again. " );
			}
		}
		input.close();
	}
}
