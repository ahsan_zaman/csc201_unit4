public class Novel extends Book{
		protected double price;
	
		public Novel(){
			price=0.0;
		}
		
		/**@param Initializing authorName who wrote the novel
		 * @param Initializing price of the novel
		 * @param Giving the novel a title 
		 * @param Novel has set number of Pages in it*/
		public Novel( String authorName, double price, String title, int numberOfPages ){
			this.authorName = authorName;
			this.price = price;
			this.title = title;
			this.numberOfPages = numberOfPages;
		}
	
		public void display(){
			System.out.println( "Author: "+authorName+"\nPrice: "+price+"\nTitle: "+title+"\nNumber of Pages: "+numberOfPages );
		}
	}
