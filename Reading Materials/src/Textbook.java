public class Textbook extends Book {
		private double price;
		public Textbook(){
			authorName = null;
			title = "MyTextbook";
			price = 0.0;
		}
		
		/**@param Number of pages that a textbook has
		 * @param the price of the textbook */
		public Textbook( int pages, double price ){
			numberOfPages = pages;
			this.price = price;
		}
	
		public void display(){
			System.out.println( "Title: "+title+"\nNumber of Pages: "+numberOfPages+"Price: "+price );
		}
	}
