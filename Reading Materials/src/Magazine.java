public class Magazine extends Literature{
		private double price;
		
		/**@param Initializing numberOfPages for the magazine with user input
		 * @param Initializing title of the magazine with user input
		 * @param Initializing price of the magazine with user input*/
		public Magazine( int numberOfPages, String title, double price ){
			this.numberOfPages = numberOfPages;
			this.title = title;
			this.price = price;
		}
	
		public void display(){
			System.out.println( "Name: "+title+"\nNumber of Pages: "+numberOfPages+"\nPrice: "+price );
		}	
	}